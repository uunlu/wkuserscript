//
//  ViewController.h
//  WKWebViewJavaScripItnject
//
//  Created by Ugur Unlu on 4/24/20.
//  Copyright © 2020 Ugur Unlu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface ViewController : UIViewController<WKScriptMessageHandler, WKNavigationDelegate, WKUIDelegate>


@end

