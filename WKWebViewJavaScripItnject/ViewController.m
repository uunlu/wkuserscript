//
//  ViewController.m
//  WKWebViewJavaScripItnject
//
//  Created by Ugur Unlu on 4/24/20.
//  Copyright © 2020 Ugur Unlu. All rights reserved.
//

#import "ViewController.h"



@interface ViewController ()

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self addWebView];
}

- (void)addWebView {
    WKPreferences *preferences = [[WKPreferences alloc] init];
    preferences.javaScriptEnabled = YES;
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    configuration.preferences = preferences;
    NSString *js = @""
    "var script = document.createElement('script');"
    "script.src = 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=default&#038;ver=1.3.8';"
    "script.type = 'text/javascript';"
    "document.getElementsByTagName('head')[0].appendChild(script);"
    "console.log('UGURU');"
    "var h1 = document.createElement('h1);"
    "document.getElementsByTagName('body')[0].appendChild(h1);"
    "";
    
  
    NSString *js3 = @"document.body.style.background = \"#ADD060\";"
    "var newDiv = document.createElement('div');"
    "var newContent = document.createTextNode('Hi there and greetings!');"
    "newDiv.appendChild(newContent);"
    "window.mraid_bridge = {"
    " getMaxSize : () => { return {a: 5}; }"
    " };"
    
    "var evt = new CustomEvent('myEvent', { detail: { name: 'Simge' } } );"
    "document.addEventListener('myEvent', (e)=> { "
    "console.log(e.detail.name);"
    "document.body.style.background = \"#DDCC60\";"
    "})"
    "";
    WKUserScript *userScript = [[WKUserScript alloc] initWithSource:js3 injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
    WKUserContentController *contentController = [[WKUserContentController alloc] init];
    [contentController removeAllUserScripts];
    [contentController addUserScript:userScript];
    [contentController addScriptMessageHandler:self name:@"TEST"];
    //    configuration.userContentController = contentController;
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, 400, 200) configuration:configuration];
    webView.UIDelegate = self;
    webView.navigationDelegate = self;
    
    [webView loadHTMLString:@"<div>UGUR</div>" baseURL:NULL];
    [webView.configuration.userContentController addUserScript:userScript];
    
    webView.configuration.userContentController = contentController;

    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [webView evaluateJavaScript:@"window.mraid_bridge.getMaxSize()" completionHandler:^(NSString *result, NSError *error)
         {
            NSLog(@"Error %@",error);
            NSLog(@"Result %@",result);
            
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self fireEvent:webView];
           });
    });
   
    
    [self.view addSubview:webView];
}

- (void)fireEvent:(WKWebView *)webView {
    NSLog(@"Firing event");
    NSString *jsStcript = @ "" // "var evt = new CustomEvent('myEvent', { detail: { name: 'Simge' } } );"
    "document.dispatchEvent(evt);"
    "";
    [webView evaluateJavaScript:jsStcript completionHandler:^(NSString *result, NSError *error)
     {
        NSLog(@"Event fired");
        NSLog(@"Error %@",error);
        NSLog(@"Result %@",result);
        
    }];
}


- (void)userContentController:(nonnull WKUserContentController *)userContentController didReceiveScriptMessage:(nonnull WKScriptMessage *)message {
    NSLog(@"%@", message);
}

@end

/*
 
var evt = new CustomEvent('myEvent', { detail: { name: 'Simge' } } )
document.addEventListener('myEvent', (e)=> { console.log(e.detail) })
 
*/
