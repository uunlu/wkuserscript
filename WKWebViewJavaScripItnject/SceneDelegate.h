//
//  SceneDelegate.h
//  WKWebViewJavaScripItnject
//
//  Created by Ugur Unlu on 4/24/20.
//  Copyright © 2020 Ugur Unlu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

